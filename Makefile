SHELL    := /bin/bash
NIL      ?= &>/dev/null
PGNAME   ?= public
WEBROOT  ?= $(HOME)/web
MKCMD    ?= hugo $(MKCMD_OPT) --minify $(NIL)
NGINXDIR ?= /var/www
SSH_KEY  ?= $(HOME)/.ssh/vultr_id_rsa
RSYNC    ?= rsync -zrvP --delete-after -e "ssh -i $(SSH_KEY)"
WEBDIR   ?= /var/www/website
TIMEOUT  ?= 2

default: $(PGNAME)

$(PGNAME): uninstall
	$(MKCMD)

.PHONY: test
test: MKCMD_OPT = -b $(WEBROOT)/$(PGNAME)
test: $(PGNAME)

.PHONY: nginx
nginx: MKCMD_OPT = -b http://127.0.0.1
nginx: $(PGNAME)
	[[ -e $(NGINXDIR) ]] || false
	pgrep nginx $(NIL) || false
	$(RSYNC) $^ $(NGINXDIR) $(NIL)

.PHONY: clean
clean:
	rm -f -- $(PGNAME)

.PHONY: install
install: $(PGNAME)
	curl -s -o /dev/null -m $(TIMEOUT) https://$(WEBSITE)/ || false
	$(RSYNC) $^/ root@$(WEBSITE):$(WEBDIR) $(NIL)

.PHONY: uninstall
uninstall:
	[[ -e $(WEBROOT)/$(PGNAME) ]] && rm -r -- $(WEBROOT)/$(PGNAME) || true
